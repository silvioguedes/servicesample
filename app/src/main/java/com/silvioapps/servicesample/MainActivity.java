package com.silvioapps.servicesample;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{
    private Button startServiceButton = null;
    private Button stopServiceButton = null;
    private Button stopThreadButton = null;

    private Button startIntentServiceButton = null;
    private Button stopIntentServiceButton = null;
    private Button stopHandleIntentButton = null;

    private MyIntentServiceConnection intentServiceConnection = null;
    private MyServiceConnection serviceConnection = null;
    private Intent intentForService = null;
    private Intent intentForIntentService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intentForService = new Intent(MainActivity.this,MyService.class);
        intentForIntentService = new Intent(MainActivity.this,MyIntentService.class);
        serviceConnection = new MyServiceConnection();
        intentServiceConnection = new MyIntentServiceConnection();

        startServiceButton = (Button)findViewById(R.id.startService);
        startServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentForService.putExtra(MyService.STOP_THREAD,false);
                startService(intentForService);
            }
        });

        stopServiceButton = (Button)findViewById(R.id.stopService);
        stopServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(intentForService);
            }
        });

        stopThreadButton = (Button)findViewById(R.id.stopThread);
        stopThreadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentForService.putExtra(MyService.STOP_THREAD,true);
                startService(intentForService);
            }
        });

        startIntentServiceButton = (Button)findViewById(R.id.startIntentService);
        startIntentServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentForIntentService.putExtra(MyIntentService.STOP_HANDLE_SERVICE,false);
                startService(intentForIntentService);
            }
        });

        stopIntentServiceButton = (Button)findViewById(R.id.stopIntentService);
        stopIntentServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(intentForIntentService);
            }
        });

        stopHandleIntentButton = (Button)findViewById(R.id.stopHandleIntent);
        stopHandleIntentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentForIntentService.putExtra(MyIntentService.STOP_HANDLE_SERVICE,true);
                startService(intentForIntentService);
            }
        });
    }

    @Override
    public void onResume() {
        if(intentForService != null && serviceConnection != null) {
            bindService(intentForService, serviceConnection, 0);
        }

        if(intentForIntentService != null && intentServiceConnection != null) {
            bindService(intentForIntentService, intentServiceConnection, 0);
        }

        super.onResume();
    }

    @Override
    protected void onPause() {
        if (serviceConnection != null) {
            unbindService(serviceConnection);
        }

        if (intentServiceConnection != null) {
            unbindService(intentServiceConnection);
        }

        super.onPause();
    }
}
