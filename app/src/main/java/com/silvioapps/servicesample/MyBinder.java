package com.silvioapps.servicesample;

import android.os.Binder;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Silvio Guedes on 02/07/2016.
 */
public class MyBinder extends Binder {
    private MyService service = null;

    public MyBinder(MyService service){
        this.service = service;

        CustomToast.showToastByHandler(service.getApplicationContext(),"MyBinder", 0);
    }

    public MyService getService(){
        CustomToast.showToastByHandler(service.getApplicationContext(),"getService", 0);

        return service;
    }
}
