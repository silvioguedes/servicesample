package com.silvioapps.servicesample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyBroadcastReceiverForIntentService extends BroadcastReceiver {
    public MyBroadcastReceiverForIntentService() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        CustomToast.showToastByHandler(context,"onReceive", 0);

        Intent newIntent = new Intent(context,MyIntentService.class);
        newIntent.putExtra(MyIntentService.STOP_HANDLE_SERVICE,false);
        context.startService(newIntent);
    }
}
