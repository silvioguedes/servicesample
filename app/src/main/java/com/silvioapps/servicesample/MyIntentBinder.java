package com.silvioapps.servicesample;

import android.os.Binder;
import android.util.Log;

/**
 * Created by Silvio Guedes on 02/07/2016.
 */
public class MyIntentBinder extends Binder {
    private MyIntentService service = null;

    public MyIntentBinder(MyIntentService service){
        CustomToast.showToastByHandler(service.getApplicationContext(),"MyIntentBinder", 0);

        this.service = service;
    }

    public MyIntentService getService(){
        CustomToast.showToastByHandler(service.getApplicationContext(),"getService", 0);

        return service;
    }
}
