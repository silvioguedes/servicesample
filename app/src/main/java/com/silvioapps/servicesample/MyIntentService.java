package com.silvioapps.servicesample;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.Context;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService extends IntentService {
    private boolean stopHandleIntent = false;
    public static final String STOP_HANDLE_SERVICE = "stopHandleIntent";
    private Handler mHandler = new Handler();

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    public void onCreate(){
        super.onCreate();

        CustomToast.showToastByHandler(getApplicationContext(),"onCreate", 0);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        if(intent != null) {
            CustomToast.showToastByHandler(getApplicationContext(),"onStartCommand", 0);

            stopHandleIntent = intent.getBooleanExtra(STOP_HANDLE_SERVICE,false);
        }

        return super.onStartCommand(intent,flags,startID);
    }

    @Override
    public IBinder onBind(Intent intent) {
        if(intent != null) {
            CustomToast.showToastByHandler(getApplicationContext(),"onBind", 0);
        }

        MyIntentBinder binder = new MyIntentBinder(this);

        return binder;
    }

    @Override
    protected void onHandleIntent(Intent intent){
        if(intent != null) {
            CustomToast.showToastByHandler(getApplicationContext(),"onHandleIntent", 0);
        }

        while (stopHandleIntent == false) {
            CustomToast.showToastByHandler(getApplicationContext(),"SERVICE RUNNING", CustomToast.TEN_SECONDS);
        }

        if(stopHandleIntent) {
            CustomToast.showToastByHandler(getApplicationContext(),"HANDLE STOPPED", 0);
        }
    }

    @Override
    public void onDestroy(){
        stopHandleIntent = true;

        CustomToast.showToastByHandler(getApplicationContext(),"onDestroy", 0);

        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        CustomToast.showToastByHandler(getApplicationContext(),"onUnbind", 0);

        return false;
    }
}
