package com.silvioapps.servicesample;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Silvio Guedes on 02/07/2016.
 */
public class MyIntentServiceConnection implements ServiceConnection {
    private MyIntentService myIntentService = null;
    private boolean isBound = false;

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder service){
        isBound = true;

        MyIntentBinder myIntentBinder = (MyIntentBinder)service;
        myIntentService = myIntentBinder.getService();

        if(myIntentService != null) {
            CustomToast.showToastByHandler(myIntentService.getApplicationContext(),"onServiceConnected", 0);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName){
        isBound = false;

        CustomToast.showToastByHandler(myIntentService.getApplicationContext(),"onServiceDisconnected", 0);
    }

    public boolean isBound(){
        return isBound;
    }
}
