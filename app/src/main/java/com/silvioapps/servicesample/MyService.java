package com.silvioapps.servicesample;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.math.BigDecimal;

public class MyService extends Service {
    private boolean stopThread = false;
    public static final String STOP_THREAD = "stopThread";
    private Thread thread = null;

    public MyService() {

    }

    @Override
    public void onCreate(){
        super.onCreate();

        CustomToast.showToastByHandler(getApplicationContext(),"onCreate",0);

        stopThread = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        if(intent != null) {
            CustomToast.showToastByHandler(getApplicationContext(),"onStartCommand", 0);

            stopThread = intent.getBooleanExtra(STOP_THREAD,false);

            thread = new Thread(){
                public void run(){
                    while(stopThread == false){
                        CustomToast.showToastByHandler(getApplicationContext(),"SERVICE RUNNING", CustomToast.TEN_SECONDS);
                    }

                    if(stopThread) {
                        CustomToast.showToastByHandler(getApplicationContext(),"THREAD STOPPED", 0);
                    }
                }
            };
            thread.start();
        }

        return super.onStartCommand(intent,flags,startID);
    }

    @Override
    public IBinder onBind(Intent intent) {
        if(intent != null) {
            CustomToast.showToastByHandler(getApplicationContext(),"onBind", 0);
        }

        MyBinder binder = new MyBinder(this);

        return binder;
    }

    @Override
    public void onDestroy(){
        stopThread = true;

        if(thread != null) {
            thread = null;
        }

        CustomToast.showToastByHandler(getApplicationContext(),"onDestroy", 0);

        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        CustomToast.showToastByHandler(getApplicationContext(),"onUnbind", 0);

        return false;
    }
}
